namespace Base2art.Aspects.Features
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Fixtures;
    using FluentAssertions;
    using Xunit;

    public class SyncTests
    {
        public object CurrentValue { get; set; }

        public string this[int Key]
        {
            get => "ssdfsdf";
            set { }
        }

        private string GetStrings(int i)
        {
            if (i == -1)
            {
                throw new ArgumentOutOfRangeException(nameof(i), "NOT_NEGATIVE");
            }

            return new string('a', i);
        }

        private int GetInt32s(int i)
        {
            if (i == -1)
            {
                throw new ArgumentOutOfRangeException(nameof(i), "NOT_NEGATIVE");
            }

            return i * i;
        }

        private string[] GetStrings() => new[] {"primary"};

        private static int callCount;

        private string GetStringWithCallCount(int i)
        {
            callCount += 1;
            return i + ":" + callCount;
        }

        private void PerformAction(int i)
        {
            Task.Delay(TimeSpan.FromMilliseconds(i)).GetAwaiter().GetResult();
        }

        [Fact]
        public void DebugExpression()
        {
            var x = Environment.TickCount % 4;
            AspectInvoker.Call(() => this.GetStrings(x));
        }

        [Fact]
        public void ShouldCallAction()
        {
            var timing = new TimingAspect();
            AspectInvoker.Call(() => this.PerformAction(2000), timing);
            timing.LastCallTime.Should().BeCloseTo(TimeSpan.FromSeconds(2), precision: 100);
        }

        [Fact]
        public void ShouldCallProperty()
        {
            var person = new Person();
            this.CurrentValue = person;

            var text = "";
            var curr = this;
            using (var sw = new StringWriter())
            {
                var loggingAspect = new LoggingAspect {Writer = sw};
                var items = AspectInvoker.Call(() => this.CurrentValue, loggingAspect, new AsserterAspect(x => x.Instance == curr));
                items.Should().Be(person);
                sw.Flush();
                text = sw.GetStringBuilder().ToString();
            }

            text.Should().Be($@"Base2art.Aspects.Features.{nameof(SyncTests)}.get_{nameof(this.CurrentValue)}(
)
  => {typeof(Person).FullName}
");
        }

        [Fact]
        public void ShouldCallPropertyWithParm()
        {
            var curr = this;
            var items = AspectInvoker.Call(() => this[3], new LoggingAspect {Writer = new StringWriter()},
                                           new AsserterAspect(x => (int) x.Arguments[0] == 3));
            items.Should().Be("ssdfsdf");
        }

        [Fact]
        public void ShouldCallWithOneAspect1()
        {
            var items = AspectInvoker.Call(() => this.GetStrings(), new CustomAspect1(), new CustomAspect2());
            items.Length.Should().Be(5);
            items[0].Should().Be("Something0");
            items[1].Should().Be("Item0");
            items[2].Should().Be("primary");
            items[3].Should().Be("Item3");
            items[4].Should().Be("Something3");
        }

        [Fact]
        public void ShouldCallWithOneAspect2()
        {
            var items = AspectInvoker.Call(() => this.GetStrings(), new CustomAspect2(), new CustomAspect1());
            items.Length.Should().Be(5);
            items[0].Should().Be("Item0");
            items[1].Should().Be("Something0");
            items[2].Should().Be("primary");
            items[3].Should().Be("Something3");
            items[4].Should().Be("Item3");
        }

        [Fact]
        public void ShouldGetActualWithNoAspects()
        {
            var items = AspectInvoker.Call(() => this.GetStrings(), null);
            items.Length.Should().Be(1);
        }

        [Fact]
        public void ShouldLog1()
        {
            var text = "";
            using (var sw = new StringWriter())
            {
                var loggingAspect = new LoggingAspect {Writer = sw};
                var items = AspectInvoker.Call(() => this.GetStrings(3), loggingAspect);
                items.Should().Be("aaa");
                sw.Flush();
                text = sw.GetStringBuilder().ToString();
            }

            text.Should().Be($@"Base2art.Aspects.Features.{nameof(SyncTests)}.{nameof(GetStrings)}(
System.Int32 i = 3
)
  => aaa
");
        }

        [Fact]
        public void ShouldLogException()
        {
            var text = "";
            using (var sw = new StringWriter())
            {
                try
                {
                    var loggingAspect = new LoggingAspect {Writer = sw};
                    AspectInvoker.Call(() => this.GetStrings(-1), loggingAspect);
                    throw new InvalidOperationException();
                }
                catch (ArgumentOutOfRangeException)
                {
                }

                sw.Flush();
                text = sw.GetStringBuilder().ToString();
            }

            text.Should().Be($@"Base2art.Aspects.Features.{nameof(SyncTests)}.{nameof(GetStrings)}(
System.Int32 i = -1
)
  => `{nameof(ArgumentOutOfRangeException)}`
");
        }

        [Fact]
        public void ShouldSkipCallOnCacheHit()
        {
            var items = AspectInvoker.Call(() => this.GetStringWithCallCount(1), new CachingAspect());
            items.Should().Be("1:1");
            items = AspectInvoker.Call(() => this.GetStringWithCallCount(1), new CachingAspect());
            items.Should().Be("1:1");

            items = AspectInvoker.Call(() => this.GetStringWithCallCount(2), new CachingAspect());
            items.Should().Be("2:2");

            items = AspectInvoker.Call(() => this.GetStringWithCallCount(2), new CachingAspect());
            items.Should().Be("2:2");

            items = this.GetStringWithCallCount(2);
            items.Should().Be("2:3");
            items = this.GetStringWithCallCount(2);
            items.Should().Be("2:4");

            items = AspectInvoker.Call(() => this.GetStringWithCallCount(2), new CachingAspect());
            items.Should().Be("2:2");
        }

        [Fact]
        public void ShouldSwallowExceptionClass()
        {
            var result = AspectInvoker.Call(() => this.GetStrings(-1), new SwallowingExceptionAspect());
            result.Should().Be(null);
        }

        [Fact]
        public void ShouldSwallowExceptionStruct()
        {
            var result = AspectInvoker.Call(() => this.GetInt32s(-1), new SwallowingExceptionAspect());
            result.Should().Be(0);
        }
    }
}
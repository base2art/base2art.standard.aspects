namespace Base2art.Aspects.Features.Contrib
{
    using System.Reflection;

    internal class MethodInterceptionArgs<T> : IMethodInterceptionArgs<T>
    {
        private readonly IMethodInterceptionArgs args;

        public MethodInterceptionArgs(IMethodInterceptionArgs args) => this.args = args;

        public Arguments Arguments => this.args.Arguments;

        public object Instance => this.args.Instance;
        public MethodInfo Method => this.args.Method;

        public T ReturnValue
        {
            get => (T) this.args.ReturnValue;
            set => this.args.ReturnValue = value;
        }

        object IMethodInterceptionArgs.ReturnValue
        {
            get => this.args.ReturnValue;
            set => this.args.ReturnValue = value;
        }
    }
}
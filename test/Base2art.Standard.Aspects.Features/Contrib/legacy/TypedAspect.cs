﻿//namespace Base2art.Aspects
//{
//    using System;
//    using System.Threading.Tasks;
//    using Internal;
//
//    public abstract class TypedAspect<T> : IAspect
//    {
//        Task IAspect.OnInvoke(IMethodInterceptionArgs args, Func<Task> next)
//        {
//            if (typeof(T).IsAssignableTo(args.Method.ReturnType))
//            {
//                return this.OnInvoke(new MethodInterceptionArgs<T>(args), async () =>
//                {
//                    await next();
//                    return (T) args.ReturnValue;
//                });
//            }
//
//            if (AspectManagers.TaskOfType<T>().IsAssignableTo(args.Method.ReturnType))
//            {
//                return this.OnInvoke(new MethodInterceptionArgs<T>(args), async () =>
//                {
//                    await next();
//                    return (T) args.ReturnValue;
//                });
//            }
//
//            return next();
//        }
//
//        protected abstract Task OnInvoke(IMethodInterceptionArgs<T> args, Func<Task<T>> next);
//    }
//}
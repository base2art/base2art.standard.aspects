namespace Base2art.Aspects.Features.Fixtures
{
    using System;
    using System.Threading.Tasks;

    public class TimingAspect : IAspect
    {
        public TimeSpan LastCallTime { get; set; }

        public async Task OnInvoke(IMethodInterceptionArgs args, Func<Task> next)
        {
            var start = DateTime.UtcNow;

            await next();

            var end = DateTime.UtcNow;
            this.LastCallTime = end - start;
        }
    }
}
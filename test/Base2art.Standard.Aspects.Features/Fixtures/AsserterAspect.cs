namespace Base2art.Aspects.Features.Fixtures
{
    using System;
    using System.Threading.Tasks;
    using FluentAssertions.Execution;

    public class AsserterAspect : IAspect
    {
        private readonly Func<IMethodInterceptionArgs, bool> func;

        public AsserterAspect(Func<IMethodInterceptionArgs, bool> func) => this.func = func;

        public Task OnInvoke(IMethodInterceptionArgs args, Func<Task> next)
        {
            if (!this.func(args))
            {
                throw new AssertionFailedException("Assertion Failed");
            }

            return next();
        }
    }
}
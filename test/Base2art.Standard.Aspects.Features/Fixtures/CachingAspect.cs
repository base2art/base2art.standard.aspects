﻿namespace Base2art.Aspects.Features.Fixtures
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class CachingAspect : IAspect
    {
        private static readonly Dictionary<string, object> Cache = new Dictionary<string, object>();

        public async Task OnInvoke(IMethodInterceptionArgs args, Func<Task> next)
        {
            var key = this.GetHash(args);
            if (Cache.ContainsKey(key))
            {
                args.ReturnValue = Cache[key];
                return;
            }

            await next();
            Cache[key] = args.ReturnValue;
        }

        private string GetHash(IMethodInterceptionArgs args)
        {
            var start = args.Method.DeclaringType.FullName + "." + args.Method.Name + "(";

            foreach (var parameter in args.Method.GetParameters())
            {
                start += parameter.ParameterType.FullName + ", ";
            }

            start += ")";

            for (var i = 0; i < args.Arguments.Count; i++)
            {
                var curr = args.Arguments[i];
                start += curr + "::✓✓::";
            }

            return start;
        }
    }
}
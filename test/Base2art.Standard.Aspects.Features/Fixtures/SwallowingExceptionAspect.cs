namespace Base2art.Aspects.Features.Fixtures
{
    using System;
    using System.Threading.Tasks;

    public class SwallowingExceptionAspect : IAspect
    {
        public async Task OnInvoke(IMethodInterceptionArgs args, Func<Task> next)
        {
            try
            {
                await next();
            }
            catch (Exception)
            {
                args.ReturnValue = null;
            }
        }
    }
}
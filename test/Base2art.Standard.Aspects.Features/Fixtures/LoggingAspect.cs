namespace Base2art.Aspects.Features.Fixtures
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public class LoggingAspect : IAspect
    {
        private TextWriter writer;

        public TextWriter Writer
        {
            get => this.writer ?? Console.Out;
            set => this.writer = value;
        }

        public async Task OnInvoke(IMethodInterceptionArgs args, Func<Task> next)
        {
            this.Writer.WriteLine(args.Method.DeclaringType + "." + args.Method.Name + "(");
            var infos = args.Method.GetParameters();
            for (var index = 0; index < infos.Length; index++)
            {
                var parameterInfo = infos[index];
                var argument = args.Arguments[index];
                this.Writer.WriteLine(parameterInfo.ParameterType.FullName + " " + parameterInfo.Name + " = " + argument);
            }

            this.Writer.WriteLine(")");

            try
            {
                await next();
            }
            catch (Exception e)
            {
                this.Writer.WriteLine($"  => `{e.GetType().Name}`");
                throw;
            }

            this.Writer.WriteLine("  => " + args.ReturnValue);
        }
    }
}
namespace Base2art.Aspects.Features.Fixtures
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class CustomAspect2 : IAspect //TypedAspect<string[]>
    {
        public async Task OnInvoke(IMethodInterceptionArgs args, Func<Task> next)
        {
            await next();

            var result = args.ReturnValue;

            var strs = new List<string>((string[]) result);
            strs.Insert(0, "Something0");
            strs.Add("Something3");
            args.ReturnValue = strs.ToArray();
        }
    }
}
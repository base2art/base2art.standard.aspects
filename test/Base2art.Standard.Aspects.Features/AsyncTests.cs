namespace Base2art.Aspects.Features
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Fixtures;
    using FluentAssertions;
    using Xunit;

    public class AsyncTests
    {
        private Task<string> GetStrings(int i)
        {
            if (i == -1)
            {
                throw new ArgumentOutOfRangeException(nameof(i), "NOT_NEGATIVE");
            }

            return Task.FromResult(new string('a', i));
        }

        private Task<int> GetInt32s(int i)
        {
            if (i == -1)
            {
                throw new ArgumentOutOfRangeException(nameof(i), "NOT_NEGATIVE");
            }

            return Task.FromResult(i * i);
        }

        private Task<string[]> GetStrings() => Task.FromResult(new[] {"primary"});

        private static int callCount;

        private Task<string> GetStringWithCallCount(int i)
        {
            callCount += 1;
            return Task.FromResult(i + ":" + callCount);
        }

        private async Task PerformAction(int i)
        {
            await Task.Delay(TimeSpan.FromMilliseconds(i));
        }

        [Fact]
        public async void DebugExpression()
        {
            var x = Environment.TickCount % 4;
            await AspectInvoker.CallAsync(() => this.GetStrings(x));
        }

        [Fact]
        public async void ShouldCallAction()
        {
            var timing = new TimingAspect();
            await AspectInvoker.CallAsync(() => this.PerformAction(2000), timing);
            timing.LastCallTime.Should().BeCloseTo(TimeSpan.FromSeconds(2), precision:100);
        }

        [Fact]
        public async void ShouldCallWithOneAspect1()
        {
            var items = await AspectInvoker.CallAsync(() => this.GetStrings(), new CustomAspect1(), new CustomAspect2());
            items.Length.Should().Be(5);
            items[0].Should().Be("Something0");
            items[1].Should().Be("Item0");
            items[2].Should().Be("primary");
            items[3].Should().Be("Item3");
            items[4].Should().Be("Something3");
        }

        [Fact]
        public async void ShouldCallWithOneAspect2()
        {
            var items = await AspectInvoker.CallAsync(() => this.GetStrings(), new CustomAspect2(), new CustomAspect1());
            items.Length.Should().Be(5);
            items[0].Should().Be("Item0");
            items[1].Should().Be("Something0");
            items[2].Should().Be("primary");
            items[3].Should().Be("Something3");
            items[4].Should().Be("Item3");
        }

        [Fact]
        public async void ShouldGetActualWithNoAspects()
        {
            var items = await AspectInvoker.CallAsync(() => this.GetStrings(), null);
            items.Length.Should().Be(1);
        }

        [Fact]
        public async void ShouldLog1()
        {
            var text = "";
            using (var sw = new StringWriter())
            {
                var loggingAspect = new LoggingAspect
                                    {
                                        Writer = sw
                                    };
                var items = await AspectInvoker.CallAsync(() => this.GetStrings(3), loggingAspect);
                items.Should().Be("aaa");
                sw.Flush();
                text = sw.GetStringBuilder().ToString();
            }

            text.Should().Be($@"Base2art.Aspects.Features.{nameof(AsyncTests)}.{nameof(GetStrings)}(
System.Int32 i = 3
)
  => aaa
");
        }

        [Fact]
        public async void ShouldLogException()
        {
            var text = "";
            using (var sw = new StringWriter())
            {
                try
                {
                    var loggingAspect = new LoggingAspect {Writer = sw};
                    await AspectInvoker.CallAsync(() => this.GetStrings(-1), loggingAspect);
                    throw new InvalidOperationException();
                }
                catch (ArgumentOutOfRangeException)
                {
                }

                sw.Flush();
                text = sw.GetStringBuilder().ToString();
            }

            text.Should().Be($@"Base2art.Aspects.Features.{nameof(AsyncTests)}.{nameof(GetStrings)}(
System.Int32 i = -1
)
  => `{nameof(ArgumentOutOfRangeException)}`
");
        }

        [Fact]
        public async void ShouldSkipCallOnCacheHit()
        {
            var items = await AspectInvoker.CallAsync(() => this.GetStringWithCallCount(1), new CachingAspect());
            items.Should().Be("1:1");
            items = await AspectInvoker.CallAsync(() => this.GetStringWithCallCount(1), new CachingAspect());
            items.Should().Be("1:1");

            items = await AspectInvoker.CallAsync(() => this.GetStringWithCallCount(2), new CachingAspect());
            items.Should().Be("2:2");

            items = await AspectInvoker.CallAsync(() => this.GetStringWithCallCount(2), new CachingAspect());
            items.Should().Be("2:2");

            items = await this.GetStringWithCallCount(2);
            items.Should().Be("2:3");
            items = await this.GetStringWithCallCount(2);
            items.Should().Be("2:4");

            items = await AspectInvoker.CallAsync(() => this.GetStringWithCallCount(2), new CachingAspect());
            items.Should().Be("2:2");
        }

        [Fact]
        public async void ShouldSwallowExceptionClass()
        {
            var result = await AspectInvoker.CallAsync(() => this.GetStrings(-1), new SwallowingExceptionAspect());
            result.Should().Be(null);
        }

        [Fact]
        public async void ShouldSwallowExceptionStruct()
        {
            var result = await AspectInvoker.CallAsync(() => this.GetInt32s(-1), new SwallowingExceptionAspect());
            result.Should().Be(0);
        }
    }
}
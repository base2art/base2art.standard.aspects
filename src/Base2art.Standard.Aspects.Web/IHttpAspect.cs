﻿namespace Base2art.Aspects.Web
{
    using System;
    using System.Threading.Tasks;
    using Base2art.Web.Http;

    public interface IHttpAspect
    {
        Task OnInvoke(IMethodInterceptionArgs args, IHttpRequest request, IHttpResponse response, Func<Task> next);
    }
}
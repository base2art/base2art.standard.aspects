﻿namespace Base2art.Aspects
{
    using System;
    using System.Threading.Tasks;

    public interface IAspect
    {
        Task OnInvoke(IMethodInterceptionArgs args, Func<Task> next);
    }
}
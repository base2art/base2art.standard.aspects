﻿namespace Base2art.Aspects
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public class Arguments : IEnumerable<object>
    {
        private readonly object[] arr = new object[0];

        public Arguments()
        {
        }

        private Arguments(int count, ParameterInfo[] parameters, object[] arguments)
        {
            this.arr = new object[count];
            for (var i = 0; i < arguments.Length; i++)
            {
                this.arr[i] = arguments[i];
            }
        }

        public int Count => this.arr.Length;

        public object this[int i]
        {
            get => this.arr[i];
            set => this.arr[i] = value;
        }

        public IEnumerator<object> GetEnumerator() => this.arr.AsEnumerable().GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        public static Arguments Create(ParameterInfo[] getParameters, object[] arguments)
        {
            var args = new Arguments(getParameters.Length, getParameters, arguments ?? new object[0]);

            return args;
        }

        internal object[] Collect() => this.arr;
    }
}
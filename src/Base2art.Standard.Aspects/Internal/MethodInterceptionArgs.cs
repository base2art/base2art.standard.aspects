﻿namespace Base2art.Aspects.Internal
{
    using System.Reflection;

    internal class MethodInterceptionArgs : IMethodInterceptionArgs
    {
        public Arguments Arguments { get; set; }
        public object ReturnValue { get; set; }
        public object Instance { get; set; }
        public MethodInfo Method { get; set; }
    }
}
namespace Base2art.Aspects
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Threading.Tasks;
    using Internal;

    public static class AspectInvoker
    {
        public static async Task<T> CallAsync<T>(this Expression<Func<Task<T>>> call, params IAspect[] aspects)
        {
            var data = ParseExpression(call);
            return await CallAsync<T>(data.Item1, data.Item2, data.Item3, aspects);
        }

        public static async Task CallAsync(this Expression<Func<Task>> call, params IAspect[] aspects)
        {
            var data = ParseExpression(call);
            await CallAsync<object>(data.Item1, data.Item2, data.Item3, aspects);
        }

        public static T Call<T>(this Expression<Func<T>> call, params IAspect[] aspects)
        {
            var data = ParseExpression(call);
            return CallAsync<T>(data.Item1, data.Item2, data.Item3, aspects).GetAwaiter().GetResult();
        }

        public static void Call(this Expression<Action> call, params IAspect[] aspects)
        {
            var data = ParseExpression(call);
            CallAsync<object>(data.Item1, data.Item2, data.Item3, aspects).GetAwaiter().GetResult();
        }

        public static async Task<T> CallAsync<T>(this object instance, MethodInfo methodInfo, object[] arguments, params IAspect[] aspects)
        {
            var item = new MethodInterceptionArgs
                       {
                           Arguments = Arguments.Create(methodInfo.GetParameters(), arguments),
                           Instance = instance,
                           Method = methodInfo,
                           ReturnValue = null
                       };

            Func<Task> backingAction = async () =>
            {
                try
                {
                    var result = methodInfo.Invoke(item.Instance, item.Arguments.Collect());

                    if (result is Task task)
                    {
                        if (methodInfo.ReturnType.GetTypeInfo().IsGenericType)
                        {
                            result = await (Task<T>) task;
                        }
                        else
                        {
                            await task;
                        }
                    }

                    // NOT AN ELSE IF...
                    if (result is T objResult)
                    {
                        item.ReturnValue = objResult;
                    }
                }
                catch (TargetInvocationException e)
                {
                    throw e.InnerException;
                }
            };

            foreach (var aspectInstance in aspects ?? new IAspect[0])
            {
                var action1 = backingAction;
                backingAction = async () =>
                {
                    var action = action1;
                    await aspectInstance.OnInvoke(item, async () => await action());
                };
            }

            await backingAction();

            if (item.ReturnValue != null)
            {
                return (T) item.ReturnValue;
            }

            return default;
        }

        private static Tuple<object, MethodInfo, object[]> ParseExpression(Expression call)
        {
            if (call is LambdaExpression lambdaExpression)
            {
                if (lambdaExpression.Body is MethodCallExpression methodCallExpressionInner)
                {
                    var methodCallExpression = methodCallExpressionInner;

                    var subject = GetValue(methodCallExpression.Object);
                    var arguments = methodCallExpression.Arguments;
                    var args = arguments.Select(GetValue).ToArray();

                    var instance = subject;
                    return Tuple.Create(instance, methodCallExpression.Method, args);
                }

                if (lambdaExpression.Body is MemberExpression memberExpressionInner)
                {
                    var prop = (PropertyInfo) memberExpressionInner.Member;
                    var getMethod = prop.GetMethod;

                    var subject = (ConstantExpression) memberExpressionInner.Expression;

                    var instance = GetValue(subject);

                    return Tuple.Create(instance, getMethod, new object[0]);
                }
            }

            throw new ArgumentOutOfRangeException($"{call.NodeType} Not supported;");
        }

        private static object GetValue(Expression argument)
        {
            if (argument is ConstantExpression ce)
            {
                return ce.Value;
            }

            var objectMember = Expression.Convert(argument, typeof(object));

            var getterLambda = Expression.Lambda<Func<object>>(objectMember);

            var getter = getterLambda.Compile();

            return getter();
        }
    }
}
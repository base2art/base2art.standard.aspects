﻿namespace Base2art.Aspects
{
    using System.Reflection;

    public interface IMethodInterceptionArgs
    {
        Arguments Arguments { get; }
        object ReturnValue { get; set; }
        object Instance { get; }
        MethodInfo Method { get; }
    }

    public interface IMethodInterceptionArgs<T> : IMethodInterceptionArgs
    {
        new T ReturnValue { get; set; }
    }
}